#!/usr/bin/env python
# coding: utf-8

# In[1]:


import mysql.connector


# In[2]:


mydb = mysql.connector.connect(
    host = "localhost",
    user = "root",
    password = "Shahimaria@123",
)

print(mydb)


# In[4]:


#creating a database
mycursor = mydb.cursor()
mycursor.execute("CREATE DATABASE newDatabase")


# In[5]:


#view all the databases
mycursor = mydb.cursor()
mycursor.execute("SHOW DATABASES")
myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# In[6]:


#connect to a database
mydb = mysql.connector.connect(
    host = "localhost",
    user = "root",
    password = "Shahimaria@123",
    database = "paf_project"
)

if(mydb):
    print("Success!!")
print(mydb)


# In[8]:


#use database command
mycursor = mydb.cursor()
mycursor.execute("USE paf_project")
myresult = mycursor.fetchall()
print(myresult)


# In[9]:


#view all tables
mycursor.execute("SHOW TABLES")
myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# In[11]:


#create a table
mycursor.execute("CREATE TABLE Backer(backerID int AUTO_INCREMENT PRIMARY KEY, name varchar(20), address varchar(50))")


# In[12]:


#view all tables
mycursor.execute("SHOW TABLES")
myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# In[13]:


#insert to table
sql = "INSERT into backer(name,address) values (%s,%s)"
val = ("David","California")

mycursor.execute(sql,val)
mydb.commit()
print(mycursor.rowcount, "record(s) inserted.")


# In[14]:


#insert multiple values
sql = "INSERT into backer(name,address) values (%s,%s)"
val = [("Maheshi","Sri Lanka"),("Anna","Russia")]

mycursor.executemany(sql,val)
mydb.commit()
print(mycursor.rowcount, "record(s) inserted.")


# In[15]:


#select * from backer
mycursor.execute("SELECT * FROM Backer")
myresult = mycursor.fetchall()

print(myresult)


# In[16]:


#select * from concept
mycursor.execute("SELECT * from concept")
myresult = mycursor.fetchall()

for x in myresult:
    print(x)


# In[17]:


#select * from backs
mycursor.execute("SELECT * from backs")
myresult = mycursor.fetchall()

for x in myresult:
    print(x)


# In[18]:


#to limit the search
mycursor.execute("SELECT * from backs LIMIT 2")
myresult = mycursor.fetchall()

for x in myresult:
    print(x)


# In[23]:


#filter conditions
mycursor.execute("SELECT * from backs where pledgeAmnt > 5000")
myresult = mycursor.fetchall()

for x in myresult:
    print(x)
    


# In[28]:


#update
mycursor.execute("UPDATE Backer SET address='Australia' WHERE backerId = 2")
mydb.commit()
print(mycursor.rowcount, "record(s) updated.")


# In[29]:


#delete
mycursor.execute("DELETE FROM Backer WHERE backerId = 2")
mydb.commit()
print(mycursor.rowcount, "record(s) deleted.")


# In[32]:


mycursor.execute("SELECT conceptName from concept where manufactID = 1")
myresult = mycursor.fetchall()

for x in myresult:
    print(x)


# In[33]:


len(myresult)


# In[34]:


myresult[1]


# In[35]:


manufactIdOne = {}
for i in range(len(myresult)):
    for line in myresult:
        items = {}
        # line = map(int, line.split())  #convert the text data to integers
        #key, value = line[0], line[1:]
        items['name'] = line[0]
    
    manufactIdOne[i] = items


# In[36]:


manufactIdOne


# In[37]:


#convert to json
import json

manufact_json = json.dumps(manufactIdOne)
print(manufact_json)


# In[38]:


mycursor.execute("SELECT backerID, pledgeAmnt from backs")
myresult = mycursor.fetchall()
backers = myresult
for x in myresult:
    print(x)


# In[39]:


backer_det = {}
for i in range(len(backers)):
    for line in backers:
        items = {}
        # line = map(int, line.split())  #convert the text data to integers
        #key, value = line[0], line[1:]
        items['backer'] = line[0]
        items['amount'] = line[1]
    
    backer_det[i] = items


# In[40]:


backer_det[i]


# In[43]:


mycursor.execute("SELECT backerID, pledgeAmnt from backs")
myresult = mycursor.fetchall()
backers_new = myresult
for x in myresult:
    print(x)


# In[44]:


mycursor.execute("SELECT conceptName from concept LIMIT 3")
myresult = mycursor.fetchall()
concept_new = myresult
for x in myresult:
    print(x)


# In[48]:


results = {}
results["Q1"] = backers_new
results["Q2"] = concept_new


# In[49]:


results


# In[50]:


list1 = concept_new


# In[51]:


list1


# In[52]:


list2 = backers_new


# In[53]:


list2


# In[54]:


list3 = backers_new


# In[55]:


list3


# In[57]:


print(list2.append(list1))


# In[58]:


list_set = list1

import csv
with open('file.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(list_set)


# In[ ]:




